//
//  BoardImageView.h
//  Monopoly
//
//  Created by Lionel on 24/09/2015.
//  Copyright © 2015 ValTechTraining. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Joueur.h"
#import "PinLayer.h"

@interface BoardImageView : UIImageView
@property(strong) NSMutableArray* pinLayers;

-(void) createPlayersPins : (NSMutableArray*) players;
-(void) movePlayerPin : (Joueur*) joueur;
@end
