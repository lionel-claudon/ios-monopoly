//
//  ViewController.h
//  MonopolyIOS
//
//  Created by Training on 15/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MonopolyRank.h"
#import "MonopolyWithListener.h"
#import "PinLayer.h"
#import "BoardImageView.h"
#import "BankLayer.h"

@interface ViewController : UIViewController
@property(strong) MonopolyRank* ranks;
@property(strong) MonopolyWithListener* monopoly;
@end

