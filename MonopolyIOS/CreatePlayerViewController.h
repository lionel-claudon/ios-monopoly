//
//  CreatePlayerViewController.h
//  Monopoly
//
//  Created by Training on 16/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MonopolyQuerys.h"
#import "SwipeableCell.h"
#import "GradientLayer.h"

@class CreatePlayerViewController;

@protocol CreatePlayerViewControllerDelegate
-(void) createPlayerReturn;
@end

@interface CreatePlayerViewController : UIViewController
@property(strong) NSString* playerName;
@property(strong) UIImage* playerAvatar;
@property(strong) id<CreatePlayerViewControllerDelegate> delegate;
@property(strong) NSMutableArray* registeredPlayers;
@property(strong) MonopolyQuerys* querys;
@end
