//
//  PrefsViewController.m
//  Monopoly
//
//  Created by Training on 16/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import "PrefsViewController.h"

@interface PrefsViewController ()
@property (weak, nonatomic) IBOutlet UISlider *playersSlider;
@property (weak, nonatomic) IBOutlet UITextField *playersTextField;
@property (weak, nonatomic) IBOutlet UITextField *roundsTextField;
@property (weak, nonatomic) IBOutlet UISlider *roundsSlider;

@end

@implementation PrefsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    GradientLayer *gradientLayer = [GradientLayer new];
    gradientLayer.frame = self.view.bounds;
    
    [self.view.layer insertSublayer:gradientLayer atIndex:0];
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"background.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    // Do any additional setup after loading the view.
    self.playersSlider.value = _numPlayers;
    self.playersTextField.text = [[NSString alloc] initWithFormat : @"%d", _numPlayers];
    self.roundsSlider.value = _numRounds;
    self.roundsTextField.text = [[NSString alloc] initWithFormat : @"%d", _numRounds];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onRoundsSliderValueChanged:(UISlider*) sender {
    float roundedValue = roundf(sender.value);
    self.roundsSlider.value = roundedValue;
    self.roundsTextField.text = [[NSString alloc] initWithFormat : @"%d", (int) roundedValue];
}
- (IBAction)onNumPlayersSliderValueChanged:(UISlider*)sender {
    float roundedValue = roundf(sender.value);
    self.playersSlider.value = roundedValue;
    self.playersTextField.text = [[NSString alloc] initWithFormat : @"%d", (int) roundedValue];
}

- (IBAction)onNumPlayersTouchUp:(id)sender {
    if(self.delegateWithBlock) {
        self.delegateWithBlock((int) _roundsSlider.value, (int) _playersSlider.value);
    }
}

- (IBAction)onNumRoundsTouchUp:(id)sender {
    if(self.delegateWithBlock) {
        self.delegateWithBlock((int) _roundsSlider.value, (int) _playersSlider.value);
    }
}

- (IBAction)submitPrefs:(UIButton *)sender {
    [self.delegate prefsViewControllerDidFinish];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
