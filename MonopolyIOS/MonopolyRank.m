//
//  MonopolyRank.m
//  Monopoly
//
//  Created by Training on 15/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import "MonopolyRank.h"

@implementation MonopolyRank

-(id) initWithRoundNumber : (int) roundNumber {
    self = [super init];
    self.roundNumber = roundNumber;
    return self;
}

@end
