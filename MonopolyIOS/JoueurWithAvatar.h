//
//  JoueurWithAvatar.h
//  Monopoly
//
//  Created by Training on 16/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "Joueur.h"

@interface JoueurWithAvatar : Joueur
@property(strong) NSData* avatar;
@end
