
//
//  ViewController.m
//  MonopolyIOS
//
//  Created by Training on 15/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import "ViewController.h"
#import "MonopolyWithListener.h"
#import "PrefsViewController.h"
#import "PlayerDescriptionViewController.h"
#import "CreatePlayerViewController.h"
#import "GradientLayer.h"

@interface ViewController () <MonopolyDelegate, UITableViewDelegate, UITableViewDataSource, PrefsViewControllerDelegate, PlayerDescritpionViewControllerDelegate, CreatePlayerViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *roundLabel;
@property (strong, nonatomic) IBOutlet UITableView *rankTable;
@property (weak, nonatomic) IBOutlet BoardImageView *gameBoardImageView;
@property(weak, nonatomic) NSMutableDictionary* pinLayersDictionary;
@property(strong, nonatomic) BankLayer* bankLayer;
@property (weak, nonatomic) IBOutlet UIButton *startMonopolyButton;
@property (weak, nonatomic) IBOutlet UIButton *prefsButton;
@property (weak, nonatomic) IBOutlet UIButton *createPlayerButton;
@end


@implementation ViewController
int numPlayers = 6;
int numRounds = 20;

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if(_ranks) {
        return _ranks.rankedPlayers.count;
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"rankCellID" forIndexPath:indexPath];
    
    [self setBackgroundLayerToCell:cell atIndexPath:indexPath];
    [self tableView:tableView configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

-(void) setBackgroundLayerToCell : (UITableViewCell*) cell atIndexPath: (NSIndexPath*) indexPath {    
    NSPredicate *backgroundLayerSelector = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        NSString *name = ((CALayer*) evaluatedObject).name;
        return [name isEqual: @"customBackground"];
    }];
    
    NSArray *backgroundLayer = [cell.contentView.layer.sublayers filteredArrayUsingPredicate:backgroundLayerSelector];
    
    if(backgroundLayer.count > 0) {
        UIColor* color = [self colorForIndex:indexPath.row];
        UIColor *colorZero = [color colorWithAlphaComponent:0.0f];
        UIColor *colorOne = [color colorWithAlphaComponent:1.0f];
        UIColor *colorTwo = [color colorWithAlphaComponent:0.0f];
        NSArray *colors =  [NSArray arrayWithObjects:(id)colorZero.CGColor, colorOne.CGColor, colorTwo.CGColor, nil];
        ((CAGradientLayer*) backgroundLayer[0]).colors = colors;
    } else {
        UIColor* color = [self colorForIndex:indexPath.row];
        UIColor *colorZero = [color colorWithAlphaComponent:0.0f];
        UIColor *colorOne = [color colorWithAlphaComponent:1.0f];
        UIColor *colorTwo = [color colorWithAlphaComponent:0.0f];
        NSArray *colors =  [NSArray arrayWithObjects:(id)colorZero.CGColor, colorOne.CGColor, colorTwo.CGColor, nil];
        
        NSNumber *stopZero = [NSNumber numberWithFloat:0.05f];
        NSNumber *stopOne = [NSNumber numberWithFloat:0.1f];
        NSNumber *stopTwo = [NSNumber numberWithFloat:0.8f];
        NSArray *locations = [NSArray arrayWithObjects:stopZero,stopOne, stopTwo, nil];
        
        CAGradientLayer *gradientLayer = [CAGradientLayer layer];
        gradientLayer.startPoint = CGPointMake(0.0f, 0.5f);
        gradientLayer.endPoint = CGPointMake(1.0f, 0.5f);
        gradientLayer.colors = colors;
        gradientLayer.locations = locations;
        gradientLayer.bounds = cell.frame;
        gradientLayer.anchorPoint = CGPointMake(0,0);
        gradientLayer.name = @"customBackground";
        [cell.contentView.layer insertSublayer:gradientLayer atIndex:0] ;
    }
}

- (void)tableView:(UITableView *)tableView configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    if(_ranks) {
        int row = (int) indexPath.row;
        Joueur* joueur = [_ranks.rankedPlayers objectAtIndex:row];
        NSString* rank = [[NSString alloc] initWithFormat: @"       %d. %@ - %d €",row+1, [joueur getNom], [joueur getCash]];
        if([joueur isKindOfClass: [JoueurWithAvatar class]]) {
            NSData* avatar = ((JoueurWithAvatar*) joueur).avatar;
            cell.imageView.image = [[UIImage alloc] initWithData:avatar];
            rank = [[NSString alloc] initWithFormat: @"%d. %@ - %d €",row+1, [joueur getNom], [joueur getCash]];
        } else {
            cell.imageView.image = nil;
        }
        
        cell.textLabel.text = rank;
    }
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
}

-(UIColor*)colorForIndex:(NSInteger) index {
    Joueur* joueur = [_ranks.rankedPlayers objectAtIndex:index];
    return joueur.pion.pinColor;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _bankLayer = [[BankLayer alloc] init];
    CGFloat x = self.gameBoardImageView.frame.origin.x + self.gameBoardImageView.frame.size.width/2;
    CGFloat y = self.gameBoardImageView.frame.origin.y + self.gameBoardImageView.frame.size.height/2;
    _bankLayer.position = CGPointMake(x,y);
    [self.view.layer addSublayer:_bankLayer];
  
    GradientLayer *gradientLayer = [GradientLayer new];
    gradientLayer.frame = self.view.bounds;
    
    [self.view.layer insertSublayer:gradientLayer atIndex:0];
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"background.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    self.view.alpha = 1;
    
    UIImage* board = [UIImage imageNamed:@"monopoly_board.png"];
    self.gameBoardImageView.image = board;
    self.gameBoardImageView.alpha = 0.8;

    // Do any additional setup after loading the view, typically from a nib.
    _monopoly = [[MonopolyWithListener alloc] initWithNumPlayers : numPlayers numTours : numRounds];
    _monopoly.delegate = self;
    [_monopoly.banque setDelegate:_bankLayer];
    [self prepareGame];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)runMonopoly:(id)sender {
    NSThread* evtThread = [ [NSThread alloc] initWithTarget:self
                                                   selector:@selector( startMonopoly )
                                                     object:nil ];
    
    [ evtThread start ];
}

-(void) startMonopoly {
    // UI Related
    dispatch_sync(dispatch_get_main_queue(), ^{
        self.startMonopolyButton.enabled = NO;
        self.prefsButton.enabled = NO;
        self.createPlayerButton.enabled = NO;
    });
    [self prepareGame];
    [_monopoly jouer];
}

-(void) prepareGame {
     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
         [_monopoly prepareGame : numPlayers numRounds: numRounds];
     });
}

-(void) didFinish: (NSString*) message {
    NSLog(@"%@", message);
    dispatch_sync(dispatch_get_main_queue(), ^{
        self.startMonopolyButton.enabled = YES;
        self.prefsButton.enabled = YES;
        self.createPlayerButton.enabled = YES;
    });
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:@"prefs"]) {
        PrefsViewController* dest = [segue destinationViewController];
        dest.numPlayers = numPlayers;
        dest.numRounds = numRounds;
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setDelegateWithBlock: ^(int rounds, int players) {
            NSLog(@"New Prefs Passed with Block: Players number : %d, Rounds : %d", players, rounds);
            numRounds = rounds;
            numPlayers = players;
        }];
    } else if (
        [[segue identifier] isEqualToString:@"playerDesc"]) {
        UITableViewCell *cell= (UITableViewCell*) sender;
        cell.selected = NO;
        NSIndexPath* pathOfTheCell = [self.rankTable indexPathForCell:cell];
        NSInteger rowOfTheCell = [pathOfTheCell row];
        Joueur* joueur = [_ranks.rankedPlayers objectAtIndex:rowOfTheCell];
        PlayerDescriptionViewController* controller = (PlayerDescriptionViewController*) [segue destinationViewController];
        [[segue destinationViewController] setDelegate:self];
        [controller setPlayer: joueur];
    } else if (
        [[segue identifier] isEqualToString:@"createPlayer"]) {
        [[segue destinationViewController] setDelegate:self];
    }
}

-(void) playerRoundFinished : (Joueur*) joueur newRank:(MonopolyRank *)ranks {
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self.gameBoardImageView movePlayerPin:joueur];
        [self updateRankTableAfterPlayerRound:joueur newRank:ranks];
    });
}

-(void) updateRankTableAfterPlayerRound:(Joueur*) player newRank : (MonopolyRank*) ranks {
    NSMutableArray* ranksBeforeSort = [NSMutableArray arrayWithArray:_ranks.rankedPlayers];
    _ranks = ranks;

    self.roundLabel.text = [[NSString alloc] initWithFormat:@"Round n°%d", ranks.roundNumber];
    int previousRow = (int) [ranksBeforeSort indexOfObject:player];
    int newRow = (int) [_ranks.rankedPlayers indexOfObject:player];
    if(newRow >= 0) {
        [self.rankTable beginUpdates];
        if(newRow != previousRow) {
            [self.rankTable moveRowAtIndexPath:[NSIndexPath indexPathForRow:previousRow inSection:0]
                                   toIndexPath: [NSIndexPath indexPathForRow:newRow inSection:0]];
        }
        [self.rankTable endUpdates];

        // If we use reloadData here - it will conflict with the row animation of moving
        for(int i = 0; i<_ranks.rankedPlayers.count; i++) {
            NSIndexPath* path = [NSIndexPath indexPathForRow:i inSection:0];
            UITableViewCell* cell = [self.rankTable cellForRowAtIndexPath:path];
            [self tableView:self.rankTable configureCell:cell atIndexPath:path];
        }
    }
}

-(void) playersInitialized : (NSMutableArray*) players initialRanking:(MonopolyRank *)ranking {
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self.gameBoardImageView createPlayersPins:players];
        self.roundLabel.text = [[NSString alloc] initWithFormat:@"Round n°0"];
        _ranks = ranking;
        [self.rankTable reloadData];
    });
}


-(void) prefsViewControllerDidFinish {
    [self dismissViewControllerAnimated : TRUE completion:^(void){}];
    [self prepareGame];
}

-(void) playerDescriptionReturn {
    [self dismissViewControllerAnimated : TRUE completion:^(void){}];
}

-(void) createPlayerReturn {
    [self dismissViewControllerAnimated : TRUE completion:^(void){}];
    [self prepareGame];
}

@end
