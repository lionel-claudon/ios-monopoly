//
//  AppDelegate.h
//  MonopolyIOS
//
//  Created by Training on 15/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

