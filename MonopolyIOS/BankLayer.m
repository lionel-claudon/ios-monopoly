//
//  BankLayer.m
//  Monopoly
//
//  Created by Lionel on 24/09/2015.
//  Copyright © 2015 ValTechTraining. All rights reserved.
//

#import "BankLayer.h"

@implementation BankLayer
CATextLayer* bankSoldTextLayer;
NSMutableArray* notesLayers;

-(id) init {
    if(self = [super init]) {
        self.bounds = CGRectMake(0.0f, 0.0f, 200.0f, 150.0f);
        self.backgroundColor = [[UIColor clearColor] CGColor];
        
        notesLayers = [NSMutableArray new];

        bankSoldTextLayer = [CATextLayer layer];
        bankSoldTextLayer.anchorPoint = CGPointMake(0, 0);
        bankSoldTextLayer.bounds = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, 200.0f, 50.0f);
        bankSoldTextLayer.fontSize = 30;
        bankSoldTextLayer.alignmentMode = @"center";
        bankSoldTextLayer.backgroundColor = [[UIColor clearColor] CGColor];
        bankSoldTextLayer.foregroundColor = [[UIColor blackColor] CGColor];
    }
    
    [self addSublayer:bankSoldTextLayer];
    
    return self;
}

-(void) bankAccountUpdated : (int) solde {
    dispatch_sync(dispatch_get_main_queue(), ^{
        bankSoldTextLayer.string = [[NSString alloc] initWithFormat: @"Bank: %d €", solde];
        if(solde == 0) {
            //Animate bills
            CABasicAnimation* scaleAnim = [CABasicAnimation new];
            scaleAnim.keyPath = @"transform.scale";
            scaleAnim.fromValue = [NSNumber numberWithFloat:1.0f];
            scaleAnim.toValue = [NSNumber numberWithFloat:2.0f];
            scaleAnim.repeatCount = 0;
            scaleAnim.removedOnCompletion = YES;
            scaleAnim.duration = 0.3f;
            scaleAnim.fillMode = kCAFillModeForwards;

            CABasicAnimation* scaleAnim2 = [CABasicAnimation new];
            scaleAnim2.keyPath = @"transform.scale";
            scaleAnim2.fromValue = [NSNumber numberWithFloat:1.0f];
            scaleAnim2.toValue = [NSNumber numberWithFloat:0.0f];
            scaleAnim2.repeatCount = 0;
            scaleAnim2.removedOnCompletion = YES;
            scaleAnim2.duration = 0.2f;
            scaleAnim2.fillMode = kCAFillModeBackwards;
            scaleAnim2.beginTime = scaleAnim.duration;

            CABasicAnimation* opacityAnim = [CABasicAnimation new];
            opacityAnim.keyPath = @"opacity";
            opacityAnim.fromValue = [NSNumber numberWithFloat:1.0f];
            opacityAnim.toValue = [NSNumber numberWithFloat:0.0f];
            opacityAnim.repeatCount = 0;
            opacityAnim.removedOnCompletion = YES;
            opacityAnim.duration = scaleAnim2.duration + scaleAnim.duration;
            
            [CATransaction begin];
            for(CALayer* l in notesLayers) {
                
                [l addAnimation:scaleAnim forKey:@"front"];
                 [CATransaction setCompletionBlock:^(){
                     [l removeFromSuperlayer];
                 }];
                [l addAnimation:scaleAnim2 forKey:@"back"];
                
                //[l addAnimation:opacityAnim forKey:nil];
            }
            [CATransaction commit];
            notesLayers = [NSMutableArray new];
        } else {
            int notes = (int) solde/100;
            if(notes > notesLayers.count) {
                int missingCount = notes - (int) notesLayers.count;
                for(int i=0; i<missingCount; i++) {
                    CALayer* layer = [CALayer new];
                    layer.bounds = CGRectMake(0, 0, 90.0f, 30.0f);
                    layer.shadowRadius = 3;
                    layer.shadowOpacity = 0.6;
                    layer.transform = CATransform3DMakeRotation(drand48() * 2 * M_PI, 0.0, 0.0, 1.0);
                    CGFloat x = self.bounds.origin.x + self.bounds.size.width/2;
                    CGFloat y = self.bounds.origin.y + self.bounds.size.height - 45.0f;
                    layer.position = CGPointMake(x + 30*(drand48() - 0.5) , y + 30*(drand48() - 0.5));
                    layer.contents = (id)[UIImage imageNamed:@"monopoly_money_100.png"].CGImage;
                    [notesLayers addObject:layer];
                    
                    [self addSublayer:layer];
                }
            }
        }
    });
}

- (void)animationDidStop:(CAAnimation * _Nonnull)theAnimation finished:(BOOL)flag {
    NSLog(@"SCALE 2 ANIMATION FINISHED");
}

@end
