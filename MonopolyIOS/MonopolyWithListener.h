//
//  MonopolyWithListener.h
//  Monopoly
//
//  Created by Training on 15/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Monopoly.h"
#import "MonopolyRank.h"
#import "JoueurWithAvatar.h"
#import "MonopolyQuerys.h"

@protocol MonopolyDelegate <NSObject>

-(void) didFinish: (NSString*) message;
-(void) playersInitialized : (NSMutableArray*) players initialRanking : (MonopolyRank*) ranking;
-(void) playerRoundFinished : (Joueur*) joueur newRank : (MonopolyRank*) ranks;
@end

@interface MonopolyWithListener : Monopoly
@property(weak) id<MonopolyDelegate> delegate;
@property(strong) NSMutableArray* registeredPlayers;
@property(strong) MonopolyQuerys* querys;
@property(strong) NSMutableArray* pinColors;

-(void) prepareGame : (int) numPlayers numRounds : (int) numRounds ;
@end