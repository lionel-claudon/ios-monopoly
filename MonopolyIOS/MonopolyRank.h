//
//  MonopolyRank.h
//  Monopoly
//
//  Created by Training on 15/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface MonopolyRank : NSObject
@property int roundNumber;
@property NSArray* rankedPlayers;

-(id) initWithRoundNumber : (int) roundNumber;
@end
