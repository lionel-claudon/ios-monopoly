//
//  CreatePlayerViewController.m
//  Monopoly
//
//  Created by Training on 16/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import "CreatePlayerViewController.h"

@interface CreatePlayerViewController () <UITableViewDelegate, UITableViewDataSource, TableViewCellDelegate>
@property (weak, nonatomic) IBOutlet UITableView *avatarTableView;
@property (weak, nonatomic) IBOutlet UITextField *playerNameTextField;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property(strong) NSMutableArray* imageDataArray;
@property (weak, nonatomic) IBOutlet UITableView *registeredPlayersTable;
@end

@implementation CreatePlayerViewController

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = 0;
    if([tableView isEqual:self.avatarTableView]) {
        count = self.imageDataArray.count;
    } else if ([tableView isEqual:self.registeredPlayersTable]) {
        count = self.registeredPlayers.count;
    }
    return count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell;
    if([tableView isEqual:self.avatarTableView]) {
        // create cell
        cell = [tableView dequeueReusableCellWithIdentifier:@"joueurImage" forIndexPath:indexPath];
        cell.imageView.image = [UIImage imageWithData:self.imageDataArray[indexPath.row]];
    } else if ([tableView isEqual:self.registeredPlayersTable]) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"registeredPlayerCell" forIndexPath:indexPath];
        if(self.registeredPlayers) {
            int row = (int) indexPath.row;
            JoueurWithAvatar* joueur = [self.registeredPlayers objectAtIndex:row];
            NSString* text = [[NSString alloc] initWithFormat: @"%@ - %d €",[joueur getNom], [joueur getCash]];
            NSData* avatar = ((JoueurWithAvatar*) joueur).avatar;
            cell.imageView.image = [[UIImage alloc] initWithData:avatar];
            cell.textLabel.text = text;
            ((SwipeableCell*) cell).delegate = self;
        }
        
        if(indexPath.row % 2 == 0) {
            cell.backgroundColor = [UIColor colorWithRed: 0.5 green:0.5 blue: 0.5 alpha:0.5];
        }
        
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
    }
    
    return cell;
}

- (IBAction)createPlayerReturn:(UIButton*) sender {
    if([sender.restorationIdentifier isEqual:@"ok"]) {
        if([self.playerNameTextField.text isEqual: nil] || [self.playerNameTextField.text isEqual: @""] ) {
            _playerName = nil;
        } else {
            _playerName = self.playerNameTextField.text;
        }
        _playerAvatar = self.imageView.image;
        
        if(![self weCanCreateThePlayer]) {
            NSLog(@"Player Name alreaday exists in DB!!!");
            return;
        }
        
        JoueurWithAvatar* j = [[JoueurWithAvatar alloc] initWithName: _playerName];
        j.avatar = UIImageJPEGRepresentation(_playerAvatar, 1);
        
        [self.querys writePlayer: j];
        [self loadAndPresentPlayersFromCoreData];
        
        self.playerNameTextField.text = nil;
        self.imageView.image = nil;
    } else {
        [self.delegate createPlayerReturn];
    }
}

-(BOOL) weCanCreateThePlayer {
    if(_playerName && _playerAvatar) {
        for(Joueur* j in self.registeredPlayers) {
            if([j.nom isEqualToString: _playerName]) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Create Error"
                                                                message:@"This name already exists"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                return NO;
            }
        }
        return YES;
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Create Error"
                                                        message:@"Please provide a name and an avatar"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    GradientLayer *gradientLayer = [GradientLayer new];
    gradientLayer.frame = self.view.bounds;
    
    [self.view.layer insertSublayer:gradientLayer atIndex:0];
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"background.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    self.imageDataArray = [[NSMutableArray alloc] init];
    self.querys = [[MonopolyQuerys alloc] init];
    [self loadAndPresentPlayersFromCoreData];
    
    NSURLSession* session = [NSURLSession sharedSession];
    NSURLSessionDataTask* dataTask = [session dataTaskWithURL:[NSURL URLWithString:@"http://public.valtech-training.fr/IPO/Material/data.json"] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSArray* json = [NSJSONSerialization JSONObjectWithData:data
                                                        options:0 error:nil];
        for(id object in json) {
            if([[object class] isSubclassOfClass: [NSDictionary class]]) {
                NSDictionary* dict = (NSDictionary*) object;
                NSArray* keys = dict.allKeys;
                for(NSString* key in keys) {
                    if([key isEqual : @"urls"]) {
                        for(NSString* url in dict[key]) {
                            NSLog(@"URL: %@", url);
                            [self getAndSaveImage : url];
                        }
                    }
                    //NSLog(@"Key:%@ Object:%@", key, dict[key]);
                }
            }
        }
    }];
    [dataTask resume];
}

-(void) loadAndPresentPlayersFromCoreData {
    NSMutableArray* coreDataPlayers = [self.querys readPlayers];
    self.registeredPlayers = coreDataPlayers;
    [self.registeredPlayersTable reloadData];
}

-(void) getAndSaveImage : (NSString*) url {
    NSURLSessionTask* task = [[NSURLSession sharedSession]
                              dataTaskWithURL:[NSURL URLWithString:url]
                              completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                  [self.imageDataArray addObject:data];
                                  dispatch_sync(dispatch_get_main_queue(), ^{
                                      [self.avatarTableView reloadData];
                                  });
                              }];
    [task resume];
                              
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if([tableView isEqual:self.avatarTableView]) {
        UITableViewCell *cell=[self.avatarTableView cellForRowAtIndexPath:indexPath];
        UIImage * image =cell.imageView.image;

        _imageView.image = image;
    }
}

-(void) deletePlayer:(NSIndexPath *)index {
    JoueurWithAvatar* joueur = [self.registeredPlayers objectAtIndex:index.row];
    
    [self.querys deletePlayer: joueur.nom];
    [self loadAndPresentPlayersFromCoreData];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
