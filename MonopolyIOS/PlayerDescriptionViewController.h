//
//  PlayerDescriptionViewController.h
//  Monopoly
//
//  Created by Training on 16/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JoueurWithAvatar.h"
#import "GradientLayer.h"

@protocol PlayerDescritpionViewControllerDelegate
-(void) playerDescriptionReturn;
@end

@interface PlayerDescriptionViewController : UIViewController
@property(strong) id<PlayerDescritpionViewControllerDelegate> delegate;
@property(strong) Joueur* player;
@end
