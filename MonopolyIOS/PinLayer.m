//
//  PinView.m
//  Monopoly
//
//  Created by Lionel on 24/09/2015.
//  Copyright © 2015 ValTechTraining. All rights reserved.
//

#import "PinLayer.h"

@implementation PinLayer
-(id) initWithPin : (Pin*) pin {
    if(self = [super init]) {
        self.pin = pin;
        self.frame = CGRectMake(0, 0, 10, 10);
        self.cornerRadius = 5;
        self.shadowRadius = 5;
        self.shadowOpacity = 0.9;
        self.shadowOffset = CGSizeMake(2, 3);
        self.backgroundColor = [[self.pin.pinColor colorWithAlphaComponent:1] CGColor];
    }
    return self;
}

@end
