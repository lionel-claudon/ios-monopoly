//
//  PinView.h
//  Monopoly
//
//  Created by Lionel on 24/09/2015.
//  Copyright © 2015 ValTechTraining. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Pin.h"
#import "Case.h"

@interface PinLayer : CALayer
@property(strong) Pin* pin;
@property(strong) Case* currentCase;

-(id) initWithPin : (Pin*) pin;
@end
