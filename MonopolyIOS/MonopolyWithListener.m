//
//  MonopolyWithListener.m
//  Monopoly
//
//  Created by Training on 15/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import "MonopolyWithListener.h"

@implementation MonopolyWithListener

-(id) initWithNumPlayers:(int)numPlayers numTours : (int) numTours {
    self = [super initWithNumPlayers: numPlayers numTours:numTours];
    [self getRegisteredPlayers];
    return self;
}

-(void) getRegisteredPlayers {
    self.querys = [[MonopolyQuerys alloc] init];
    self.registeredPlayers = [[NSMutableArray alloc] init];
    
    NSMutableArray* coreDataPlayers = [self.querys readPlayers];
    if(coreDataPlayers.count > 0) {
        self.registeredPlayers = coreDataPlayers;
    }
}

-(void) jouer {
    for(int round = 1; round<=self.numTours; round++) {
        [NSThread sleepForTimeInterval:0.5f];
        NSLog(@"\n\n\n\n****************   Round n° %d  ****************", round);
        for(Joueur* j in self.players) {
            [NSThread sleepForTimeInterval:0.05f];
            [j aTonTour:self.gobelet];
            int balance = [j calculateNewCash];
            if(balance >= 0) {
                NSLog(@"😄 %@ is on %@, cash: %d € (+%d €)", j.nom, j.position.nom, j.cash, balance);
            } else {
                // Put cash in Banque
                [self.banque addCash: -balance];
                NSLog(@"😢 %@ is on %@, cash: %d € (%d €)", j.nom, j.position.nom, j.cash, balance);
                NSLog(@"💰💰 %d € in the Bank, new total: %d", -balance, [self.banque getSolde]);
            }
            
            MonopolyRank* ranking = [self createMonopolyRankForRound : round];
            [self.delegate playerRoundFinished: j newRank:ranking];
        }
    }
    [self.delegate didFinish : @"Monopoly is finished!"];
}

-(MonopolyRank*) createMonopolyRankForRound : (int) roundId {
    MonopolyRank* rank = [[MonopolyRank alloc] initWithRoundNumber: roundId];

    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"cash"
                                                 ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray;
    sortedArray = [self.players sortedArrayUsingDescriptors:sortDescriptors];

    [rank setRankedPlayers: sortedArray];
    
    return rank;
}

-(void) prepareGame:(int)numPlayers numRounds : (int) numRounds {
    NSLog(@"Prepare the Game");
    self.players = [NSMutableArray new];
    self.numTours = numRounds;
    [self.banque getAllCash];
    
    NSMutableArray* coreDataPlayers = [self.querys readPlayers];
    self.registeredPlayers = coreDataPlayers;
    
    if(self.registeredPlayers.count <= numPlayers) {
        for(id j in self.registeredPlayers) {
            [self.players addObject:j];
        }
        
        for(int i=(int) self.registeredPlayers.count; i<numPlayers; i++) {
            NSLog(@"Creating PLayer n° %d", i);
            NSString* playerName = [NSString stringWithFormat:@"Player %d", i+1];
            Joueur* joueur = [[Joueur alloc] initWithName : playerName];
            [self.players addObject : joueur];
        }
    } else {
        for(int i = 0; i<numPlayers; i++) {
            [self.players addObject : [self.registeredPlayers objectAtIndex:i]];
        }
    }
    
    for(Joueur* j in self.players) {
        j.cash = 1500;
        j.position = [self.plateau getCaseInitiale];
    }
    
    [self assignPinColors];
    MonopolyRank* ranking = [self createMonopolyRankForRound : 0];
    [self.delegate playersInitialized:self.players initialRanking:ranking];
    
    //[self.delegate roundFinished: ranking];
}

-(void) assignPinColors {
    int i = 0;
    
    for(Joueur* j in self.players) {
        float hue = (float) i/self.players.count;
        UIColor* color = [[UIColor alloc] initWithHue: hue saturation: 0.5 brightness: 0.95 alpha: 0.9];
        j.pion = [[Pin alloc] initWithColor: color];
        i++;
    }
}

@end
