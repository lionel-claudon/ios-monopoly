//
//  PlayerDescriptionViewController.m
//  Monopoly
//
//  Created by Training on 16/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import "PlayerDescriptionViewController.h"

@interface PlayerDescriptionViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *playerImageView;
@property (weak, nonatomic) IBOutlet UILabel *playerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *playerCashLabel;
@property (weak, nonatomic) IBOutlet UILabel *playerPositionLabel;
@end

@implementation PlayerDescriptionViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    GradientLayer *gradientLayer = [GradientLayer new];
    gradientLayer.frame = self.view.bounds;
    
    [self.view.layer insertSublayer:gradientLayer atIndex:0];
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"background.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    if(self.player) {
        self.playerNameLabel.text = self.player.nom;
        self.playerCashLabel.text = [[NSString alloc] initWithFormat : @"%d €", self.player.cash];
        self.playerPositionLabel.text = self.player.position.nom;
        if([self.player isKindOfClass: [JoueurWithAvatar class]]) {
            NSData* avatar = ((JoueurWithAvatar*) self.player).avatar;
            self.playerImageView.image = [[UIImage alloc] initWithData:avatar];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButton:(id)sender {
    [self.delegate playerDescriptionReturn];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
