//
//  SwipeableCelleTableViewCell.h
//  Monopoly
//
//  Created by Training on 18/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import <UIKit/UIKit.h>

// A protocol that the TableViewCell uses to inform its delegate of state change
@protocol TableViewCellDelegate
    // indicates that the given item has been deleted
-(void)deletePlayer:(NSIndexPath*) index;
@end

@interface SwipeableCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UIView *myContentView;
@property (nonatomic, strong) UIPanGestureRecognizer *panRecognizer;
@property (nonatomic, assign) CGPoint panStartPoint;
@property(strong) id<TableViewCellDelegate> delegate;
@end
