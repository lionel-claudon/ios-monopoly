//
//  SwipeableCelleTableViewCell.m
//  Monopoly
//
//  Created by Training on 18/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import "SwipeableCell.h"

@interface SwipeableCell() <UIGestureRecognizerDelegate>
@end

@implementation SwipeableCell
CGPoint originalCenter;
BOOL deleteOnDragRelease;

- (void)awakeFromNib {
    // Initialization code
    self.myContentView = [self contentView];
    self.panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panThisCell:)];
    self.panRecognizer.delegate = self;
    [self.myContentView addGestureRecognizer:self.panRecognizer];
    self.selectionStyle = UITableViewCellEditingStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (void)panThisCell:(UIPanGestureRecognizer *)recognizer {
    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan:
            self.panStartPoint = self.center;
            //NSLog(@"Pan Began at %@", NSStringFromCGPoint(self.panStartPoint));
            break;
        case UIGestureRecognizerStateChanged: {
            CGPoint translation = [recognizer translationInView:self];
            self.alpha = 0.9 - 2*[self frame].origin.x / -[self frame].size.width;
            self.center = CGPointMake(self.panStartPoint.x + translation.x, self.panStartPoint.y);
            deleteOnDragRelease = [self frame].origin.x < -[self frame].size.width / 2.0;
            //NSLog(@"Pan Moved %f %f", translation.x, translation.y);
        }
            break;
        case UIGestureRecognizerStateEnded: {
            //NSLog(@"Pan Ended");
            self.alpha = 1;
            CGRect originalFrame = CGRectMake(0, [self frame].origin.y, [self bounds].size.width, [self bounds].size.height);
            if (!deleteOnDragRelease) {
                // if the item is not being deleted, snap back to the original location
                [UIView animateWithDuration:0.2 animations:^(void){self.frame = originalFrame;}];
            } else {
                UITableView* tableView = (UITableView *)self.superview.superview;
                NSIndexPath* path = [tableView indexPathForCell: self];
                [self.delegate deletePlayer:path];
            }
        }
            break;
        case UIGestureRecognizerStateCancelled:
            //NSLog(@"Pan Cancelled");
            break;
        default:
            break;
    }
}

@end
