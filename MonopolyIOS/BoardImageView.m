//
//  BoardImageView.m
//  Monopoly
//
//  Created by Lionel on 24/09/2015.
//  Copyright © 2015 ValTechTraining. All rights reserved.
//

#import "BoardImageView.h"

@implementation BoardImageView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id) initWithCoder:(NSCoder *)aDecoder {
    if(self = [super initWithCoder:aDecoder]) {
        self.pinLayers = [NSMutableArray new];
    }
    
    return self;
}

-(id) initWithFrame:(CGRect)frame {
    if(self = [super initWithFrame:frame]) {
        self.pinLayers = [NSMutableArray new];
    }
    
    return self;
}

-(void) createPlayersPins:(NSMutableArray *)players {
   
    
    // remove previous layers
    [self.layer.sublayers makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
    self.pinLayers = [NSMutableArray new];
    
    // create new layers
    for (Joueur* j in players) {
        PinLayer* layer = [[PinLayer alloc] initWithPin : j.pion];
        [layer setCurrentCase : j.position];
        [self.pinLayers addObject:layer];
        [self.layer addSublayer: layer];
    }
    
    [self drawAllPins];
}

-(void) drawAllPins {
    NSMutableDictionary* layersPerCaseDictionary = [self sortLayersPerCase];
    for(NSNumber* a in layersPerCaseDictionary.allKeys) {
        [self drawPins: [layersPerCaseDictionary objectForKey:a] atCase: a];
        
    }
}

-(NSMutableDictionary*) sortLayersPerCase {
    NSMutableDictionary* dict = [NSMutableDictionary new];
    
    for(PinLayer* pinLayer in self.pinLayers) {
        if([dict.allKeys containsObject:pinLayer.currentCase.caseId]) {
            [(NSMutableArray*) ([dict objectForKey: pinLayer.currentCase.caseId]) addObject : pinLayer];
        } else {
            NSMutableArray* array = [NSMutableArray new];
            [array addObject:pinLayer];
            [dict setObject:array forKey:pinLayer.currentCase.caseId];
        }
    }
    
    return dict;
}


-(void) drawPins : (NSMutableArray*) pinLayers atCase : (NSNumber*) position {
    CGPoint caseAnchor = [self getCaseAnchorForId : (int) position.integerValue];
    if(pinLayers.count == 1) {
        PinLayer* layer = [pinLayers objectAtIndex:0];
        layer.position = caseAnchor;
    } else {
        /*
         A point at angle theta on the circle whose centre is (x0,y0) and whose radius is r is (x0 + r cos theta, y0 + r sin theta). 
         Theta values are evenly spaced between 0 and 2pi.
         */
        int radius = 7;
        int i = 0;
        float theta;
        CGFloat x0 = caseAnchor.x;
        CGFloat y0 = caseAnchor.y;
        CGFloat x,y;
        for(PinLayer* layer in pinLayers) {
            theta = M_PI/3 + 2*M_PI * i/pinLayers.count;
            x = x0 + radius*cosf(theta);
            y = y0 + radius*sin(theta);
            
            layer.position = CGPointMake(x,y);
            i++;
        }
    }
}

-(CGPoint) getCaseAnchorForId : (int) caseId {
    float frameWidth = self.frame.size.width;
    float frameHeight = self.frame.size.height;
    
    float cornerCaseW = 0.128 * frameWidth;
    float cornerCaseH = 0.128 * frameHeight;
    float regularCaseW = 0.0824 * frameWidth;
    float regularCaseH = cornerCaseW;
    
    float x = frameWidth;
    float y = frameHeight;
    if(caseId == 0) {
        x += -cornerCaseW/2;
        y += -cornerCaseH/2;
    } else if(caseId > 0 && caseId < 10) {
        y += -regularCaseH/2;
        x += -regularCaseW/2 - cornerCaseW -((caseId % 10) - 1) * regularCaseW;
    } else if (caseId == 10) {
        y += -cornerCaseH/2;
        x += cornerCaseW/2 - self.frame.size.width;
    } else if (caseId > 10 && caseId < 20) {
        y += -regularCaseW/2 - cornerCaseH - ((caseId % 10) - 1) * regularCaseW;
        x += cornerCaseW/2 - self.frame.size.width;
    } else if(caseId == 20) {
        y += -frameHeight + cornerCaseH/2;
        x += -frameWidth + cornerCaseW/2;
    } else if (caseId > 20 && caseId < 30) {
        y += -frameHeight + regularCaseH/2;
        x += -frameWidth + cornerCaseW + ((caseId % 10) - 1) * regularCaseW + regularCaseW/2;
    } else if(caseId == 30) {
        y += -frameHeight + cornerCaseH/2;
        x += -cornerCaseW/2;
    } else if (caseId > 30 && caseId < 40) {
        y += -frameHeight + cornerCaseH + ((caseId % 10) - 1) * regularCaseW + regularCaseW/2;
        x += -regularCaseH/2;
    }
    
    return CGPointMake(x, y);
}

-(void) movePlayerPin:(Joueur *)joueur {
    // Get related pin
    PinLayer* l;
    for(PinLayer* pinLayer in self.pinLayers) {
        if([pinLayer.pin isEqual:joueur.pion]) {
            l = pinLayer;
            break;
        }
    }
    
    // Move the pins
    l.currentCase = joueur.position;
    [self drawAllPins];
}
@end
