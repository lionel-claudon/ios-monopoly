//
//  BankLayer.h
//  Monopoly
//
//  Created by Lionel on 24/09/2015.
//  Copyright © 2015 ValTechTraining. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
#import "Banque.h"

@interface BankLayer : CALayer <BanqueDelegate>

@end
