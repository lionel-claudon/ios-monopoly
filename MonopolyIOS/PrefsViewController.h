//
//  PrefsViewController.h
//  Monopoly
//
//  Created by Training on 16/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GradientLayer.h"

@class PrefsViewController;

@protocol PrefsViewControllerDelegate
-(void) prefsViewControllerDidFinish;
@end

typedef void(^Delegate)(int numRounds, int numPlayers);

@interface PrefsViewController : UIViewController
@property(strong) id<PrefsViewControllerDelegate> delegate;
@property(strong) Delegate delegateWithBlock;
@property int numRounds;
@property int numPlayers;
@end
