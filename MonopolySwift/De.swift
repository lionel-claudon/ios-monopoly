//
//  De.swift
//  Monopoly
//
//  Created by Training on 15/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

import Foundation
class De {
    var valeurFace : CInt;
    
    init() {
        valeurFace = 0
    }
    
    func lancer() {
        valeurFace = (CInt) (1+arc4random_uniform(6))
    }
    
    func getValeurFace() -> CInt {
        return valeurFace
    }
}