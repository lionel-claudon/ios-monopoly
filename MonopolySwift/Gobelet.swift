//
//  Gobelet.swift
//  Monopoly
//
//  Created by Training on 15/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

import Foundation

class Gobelet {
    let de1 : De = De()
    let de2 : De = De()
    
    func lancer() {
        de1.lancer()
        de2.lancer()
    }
    
    func getValeur() -> CInt {
        return de1.getValeurFace() + de2.getValeurFace()
    }
}