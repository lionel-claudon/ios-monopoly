//
//  Banque.swift
//  Monopoly
//
//  Created by Training on 15/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

import Foundation

class Banque : NSObject {
    var soldeBanque : CInt
    
    override init() {
        soldeBanque = 0
    }
    
    func getSolde() -> CInt {
        return soldeBanque
    }
    
    func getAllCash() -> CInt {
        var solde = soldeBanque
        soldeBanque = 0
        return solde
    }
    
    func addCash(cash:CInt) {
        soldeBanque += cash
    }
}