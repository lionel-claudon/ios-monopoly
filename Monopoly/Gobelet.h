//
//  Gobelet.h
//  Monopoly
//
//  Created by Training on 14/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "De.h"

@interface Gobelet : NSObject
@property(nonatomic, strong) De* de1;
@property(nonatomic, strong) De* de2;
@property(nonatomic, strong) NSArray* dee;

-(void) lancer;
-(int) getValeur;
@end
