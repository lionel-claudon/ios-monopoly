//
//  main.m
//  Monopoly
//
//  Created by Training on 14/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Monopoly.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        Monopoly* monopoly = [[Monopoly alloc] initWithNumPlayers : 6 numTours : 20];
        [monopoly jouer];
    }
    return 0;
}
