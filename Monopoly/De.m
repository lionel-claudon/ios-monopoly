//
//  De.m
//  Monopoly
//
//  Created by Training on 14/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import "De.h"

@implementation De
-(void) lancer {
    self.valeurFace = 1+arc4random_uniform(6);
}

-(int) getValeurFace {
    return self.valeurFace;
}

@end
