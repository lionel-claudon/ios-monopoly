//
//  Gobelet.m
//  Monopoly
//
//  Created by Training on 14/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import "Gobelet.h"

@implementation Gobelet
-(id) init {
    self = [super init];
    self.de1 = [De new];
    self.de2 = [De new];
    
    // Other syntax to initalize the array
    self.dee = @[
                 [[De alloc] init],
                 [[De alloc] init]
                 ];
    
    return self;
}

-(void) lancer {
    [self.de1 lancer];
    [self.de2 lancer];
    
    // Other syntax
    /*
     for(De* de in self.dee) {
        [self.dee enumerateObjectsUsingBlock:<#^(id obj, NSUInteger idx, BOOL *stop)block#>] {
            
        }
    }
     */
}

-(int) getValeur {
    //NSLog(@"Lancer = %d (%d + %d)",self.de1.valeurFace + self.de2.valeurFace, self.de1.valeurFace, self.de2.valeurFace);

    return self.de1.valeurFace + self.de2.valeurFace;
}

@end
