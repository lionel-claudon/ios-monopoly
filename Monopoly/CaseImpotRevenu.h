//
//  CaseImpotRevenu.h
//  Monopoly
//
//  Created by Training on 14/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import "Case.h"

@interface CaseImpotRevenu : Case
@property(readonly) int MIN_TAX;
@end
