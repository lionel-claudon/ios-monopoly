//
//  Banque.h
//  Monopoly
//
//  Created by Lionel on 15/09/15.
//  Copyright (c) 2015 Lionel. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BanqueDelegate <NSObject>
-(void) bankAccountUpdated : (int) solde;
@end

@interface Banque : NSObject
@property int soldeBanque;

-(int) getSolde;
-(int) getAllCash;
-(void) addCash : (int) cash;
@property(weak) id<BanqueDelegate> delegate;
@end
