//
//  Plateau.h
//  Monopoly
//
//  Created by Training on 14/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Case.h"
#import "CaseDepart.h"
#import "CaseImpotRevenu.h"
#import "CaseTaxeDeLuxe.h"
#import "CaseParcGratuit.h"
//#import "Monopoly-Swift.h"



@interface Plateau : NSObject {
    @private Case* caseDepart;
    @private CaseParcGratuit* caseParcGratuit;
}

-(Case*) getCaseInitiale;
-(CaseParcGratuit*) getCaseParcGratuit;
-(id) initWithNumCases : (int) numCases andBanque : (Banque*) banque;
@end
