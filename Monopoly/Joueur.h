//
//  Joueur.h
//  Monopoly
//
//  Created by Training on 14/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pin.h"
#import "Gobelet.h"
#import "Case.h"
#import "CaseDepart.h"


@interface Joueur : NSObject
@property(nonatomic, strong) NSString* nom;
@property(nonatomic, strong) Pin* pion;
@property(nonatomic, strong) Case* case_initiale;
@property(nonatomic, strong) Case* position;
@property(nonatomic) int cash;
@property(nonatomic) BOOL crossedCaseDepart;


-(id) initWithName : (NSString*) nom;
-(void) aTonTour : (Gobelet*) gob;
-(NSString*) getNom;
-(int) getCash;
-(int) calculateNewCash;

@end
