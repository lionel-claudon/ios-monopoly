//
//  Joueur.m
//  Monopoly
//
//  Created by Training on 14/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import "Joueur.h"


@implementation Joueur
// Functions starting with init* are auto detected by framework as constructors
-(id) initWithName:(NSString *) nom {
    self = [super init];
    // equals self.nom
    _nom = nom;
    _cash = 1500;
    _crossedCaseDepart = false;
    _pion = [Pin new];
    return self;
}


-(void) aTonTour : (Gobelet*) gob {
    [gob lancer];
    int result = [gob getValeur];
    
    // Move
    _crossedCaseDepart = false;
    for(int i = 0; i < result; i++) {
        self.position = self.position.suivante;
        if([self.position isKindOfClass : [CaseDepart class]] && i != result-1) {
            _crossedCaseDepart = true;
        }
    }
}

-(NSString*) getNom {
    return self.nom;
}

-(int) getCash {
    return self.cash;
}


-(int) calculateNewCash {
    int cashBalance = [self.position cashBalance: self.cash];
    if(_crossedCaseDepart) {
        cashBalance += 200;
    }
    
    self.cash += cashBalance;
    return cashBalance;
}

@end
