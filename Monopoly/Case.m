//
//  Case.m
//  Monopoly
//
//  Created by Training on 14/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import "Case.h"

@implementation Case
-(id) initWithName : (NSString*) name andId : (int) caseId {
    self = [super init];
    _nom = name;
    _caseId = [[NSNumber alloc] initWithInt: caseId];
    return self;
}

-(NSString*) getNom {
    return self.nom;
}

-(int) cashBalance:(int)playerCash {
    return 0;
}

@end