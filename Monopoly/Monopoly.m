//
//  Monopoly.m
//  Monopoly
//
//  Created by Training on 14/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import "Monopoly.h"

@implementation Monopoly
-(void) jouer {
    for(int round = 1; round<=self.numTours; round++) {
        NSLog(@"\n\n\n\n****************   Round n° %d  ****************", round);
        for(Joueur* j in self.players) {
            [j aTonTour:self.gobelet];
            int balance = [j calculateNewCash];
            if(balance >= 0) {
                NSLog(@"😄 %@ is on %@, cash: %d € (+%d €)", j.nom, j.position.nom, j.cash, balance);
            } else {
                // Put cash in Banque
                [self.banque addCash: -balance];
                NSLog(@"😢 %@ is on %@, cash: %d € (%d €)", j.nom, j.position.nom, j.cash, balance);
                NSLog(@"💰💰 %d € in the Bank, new total: %d", -balance, [self.banque getSolde]);
            }
        }
    }
}

-(id) initWithNumPlayers:(int)numPlayers numTours : (int) numTours {
    self = [super init];
    self.gobelet = [Gobelet new];
    self.players = [NSMutableArray new];
    self.numTours = numTours;
    self.banque = [Banque new];
    self.plateau = [[Plateau alloc] initWithNumCases : 40 andBanque: self.banque];
    Case* caseInitiale = [self.plateau getCaseInitiale];

    for(int i = 0; i<numPlayers; i++) {
        NSLog(@"Creating PLayer n° %d", i);
        NSString* playerName = [NSString stringWithFormat:@"Player %d", i+1];
        Joueur* joueur = [[Joueur alloc] initWithName : playerName];
        joueur.position = caseInitiale;
        [self.players addObject : joueur];
    }
    
    return self;
}

@end
