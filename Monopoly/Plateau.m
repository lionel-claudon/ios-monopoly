//
//  Plateau.m
//  Monopoly
//
//  Created by Training on 14/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import "Plateau.h"

@implementation Plateau
-(id) initWithNumCases:(int)numCases andBanque:(Banque *)banque {
    //NSLog(@"Création du plateau");
    self = [super init];
    NSString* caseName = [NSString stringWithFormat:@"Case Départ"];
    caseDepart = [[CaseDepart alloc] initWithName: caseName andId : 0];
    Case* prec = caseDepart;
    for(int i=1; i<=numCases; i++) {
        NSString* caseName = nil;
        Case* aCase = nil;
        
        if(i == 4) {
            caseName = [NSString stringWithFormat:@"Case Impôts sur le revenu"];
            aCase = [[CaseImpotRevenu alloc] initWithName : caseName andId : i];
        } else if (i==20) {
            caseName = [NSString stringWithFormat:@"Case Parc Gratuit"];
            aCase = [[CaseParcGratuit alloc] initWithName : caseName andId : i];
            caseParcGratuit = (CaseParcGratuit*) aCase;
            [caseParcGratuit setBanque : banque];
        } else if(i==38) {
            caseName = [NSString stringWithFormat:@"Case Taxe de Luxe"];
            aCase = [[CaseTaxeDeLuxe alloc] initWithName : caseName andId : i];
        } else {
            caseName = [NSString stringWithFormat:@"Case %d", i];
            aCase = [[Case alloc] initWithName : caseName andId : i];
        }
        
        if(i == numCases) {
            prec.suivante = caseDepart;
            //NSLog(@"%@ créée, suivante : %@",prec.nom, self.caseDepart.nom);

        } else {
            prec.suivante = aCase;
            //NSLog(@"%@ créée, suivante : %@",prec.nom, aCase.nom);
        }
        
        prec = aCase;
    }
    
    return self;
}

-(Case*) getCaseInitiale {
    return caseDepart;
}

-(CaseParcGratuit*) getCaseParcGratuit {
    return caseParcGratuit;
}

@end
