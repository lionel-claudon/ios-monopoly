//
//  Banque.m
//  Monopoly
//
//  Created by Lionel on 15/09/15.
//  Copyright (c) 2015 Lionel. All rights reserved.
//

#import "Banque.h"

@implementation Banque

-(id) init{
    self = [super init];
    _soldeBanque = 0;
    [self notifyDelegate];
    return self;
}

-(int) getSolde{
    return self.soldeBanque;
}

-(int) getAllCash {
    int solde = _soldeBanque;
    _soldeBanque = 0;
    [self notifyDelegate];
    return solde;
}

-(void) addCash : (int) cash {
    _soldeBanque += cash;
    [self notifyDelegate];
}

-(void) notifyDelegate {
    if(self.delegate) {
        [self.delegate bankAccountUpdated:self.soldeBanque];
    }
}

@end
