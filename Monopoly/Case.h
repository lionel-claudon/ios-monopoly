//
//  Case.h
//  Monopoly
//
//  Created by Training on 14/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Case : NSObject
@property(readonly) NSString* nom;
@property(strong) Case* suivante;
@property(strong) NSNumber* caseId;

-(id) initWithName : (NSString*) name andId : (int) caseId;
-(NSString*) getNom;
-(int) cashBalance : (int) playerCash;

@end
