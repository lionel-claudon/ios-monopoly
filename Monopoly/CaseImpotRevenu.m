//
//  CaseImpotRevenu.m
//  Monopoly
//
//  Created by Training on 14/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import "CaseImpotRevenu.h"

@implementation CaseImpotRevenu
-(id) initWithName:(NSString *) name andId:(int)caseId {
    self = [super initWithName: name andId:(int)caseId ];
    _MIN_TAX = 350;
    return self;
}
-(int) cashBalance : (int) playerCash {
    int tax = (int) (0.15*playerCash);
    BOOL aboveMinTax =  tax > self.MIN_TAX;
    return -(aboveMinTax? tax : _MIN_TAX);
}

@end
