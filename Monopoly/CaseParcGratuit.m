//
//  CaseParcGratuit.m
//  Monopoly
//
//  Created by Lionel on 15/09/15.
//  Copyright (c) 2015 Lionel. All rights reserved.
//

#import "CaseParcGratuit.h"

@implementation CaseParcGratuit

-(int) cashBalance:(int)playerCash {
    return [self.banque getAllCash];
}

@end
