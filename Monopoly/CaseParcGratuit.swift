//
//  CaseParcGratuit.swift
//  Monopoly
//
//  Created by Training on 15/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

import Foundation

class CaseParcGratuit : Case {
    var banque : Banque!
    
    override init(name:String) {
        super.init(name:name)
    }
    
    override func cashBalance(playerCash: CInt) -> CInt {
        return banque.getAllCash();
    }
}