//
//  Monopoly.h
//  Monopoly
//
//  Created by Training on 14/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Joueur.h"
#import "Gobelet.h"
#import "Plateau.h"


@interface Monopoly : NSObject
@property(strong) NSMutableArray* players;
@property(strong) Plateau* plateau;
@property(strong) Gobelet* gobelet;
@property(strong) Banque* banque;
@property int numTours;

-(void) jouer;
-(id) initWithNumPlayers : (int) numPlayers numTours : (int) numTours;
@end
