//
//  CaseParcGratuit.h
//  Monopoly
//
//  Created by Lionel on 15/09/15.
//  Copyright (c) 2015 Lionel. All rights reserved.
//

#import "Case.h"
#import "Banque.h"

@interface CaseParcGratuit : Case
@property(strong) Banque* banque;
@end
