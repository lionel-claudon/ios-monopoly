//
//  Pin.h
//  Monopoly
//
//  Created by Lionel on 24/09/2015.
//  Copyright © 2015 ValTechTraining. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Pin : NSObject
@property(strong) UIColor* pinColor;

-(id) initWithColor : (UIColor*) color;
@end
