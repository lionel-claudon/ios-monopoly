//
//  Pin.m
//  Monopoly
//
//  Created by Lionel on 24/09/2015.
//  Copyright © 2015 ValTechTraining. All rights reserved.
//

#import "Pin.h"

@implementation Pin
float goldenRatio = 0.618033988749895;

-(id) init {
    if(self = [super init]) {
        float hue = (float) drand48();
        hue += goldenRatio;
        // Get only decimal out
        hue -= floor(hue);
        self.pinColor = [[UIColor alloc] initWithHue: hue saturation: 0.5 brightness: 1 alpha: 1];
    }
    
    return self;
}

-(id) initWithColor : (UIColor*) color {
    if(self = [super init]) {
        self.pinColor = color;
    }
    return self;
}

@end
