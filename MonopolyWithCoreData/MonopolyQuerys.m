//
//  MonopolyQuerys.m
//  Monopoly
//
//  Created by Training on 18/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import "MonopolyQuerys.h"
#import "AppDelegate.h"

@implementation MonopolyQuerys
-(NSManagedObjectContext*) context {
    NSManagedObjectContext* context = nil;
    
    id delegate = [UIApplication sharedApplication].delegate;
    if([delegate respondsToSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    };
    
    return context;
}

-(NSMutableArray*) readPlayers {
    NSArray* players;
    NSMutableArray* returnablePlayers = [[NSMutableArray alloc] init];
    NSManagedObjectContext* context = [self context];
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] initWithEntityName: @"Player"];
    NSError* error = nil;
    
    players = [context executeFetchRequest:fetchRequest error:&error];
    if(players.count >0) {
        for(NSManagedObject* player in players) {
            NSString* name = [player valueForKey:@"name"];
            NSData* avatar = [player valueForKey:@"avatar"];
            int cash =  [[player valueForKey:@"cash"] intValue];
            
            JoueurWithAvatar* j = [[JoueurWithAvatar alloc] initWithName:name];
            j.avatar = avatar;
            j.cash = cash;
            
            [returnablePlayers addObject: j];
            NSLog(@"Registered Player Retrieved: %@ Cash: %d", name, cash);
        }
    }
    
    
    return returnablePlayers;
}

-(void) writePlayer : (Joueur*) joueur {
    NSManagedObjectContext* context = [self context];
    NSManagedObject* playerObject = [NSEntityDescription
                                     insertNewObjectForEntityForName:@"Player" inManagedObjectContext:context];
    [playerObject setValue:joueur.nom forKey:@"name"];
    [playerObject setValue:@(joueur.cash) forKey:@"cash"];
    
    if([joueur isKindOfClass : [JoueurWithAvatar class]]) {
        [playerObject setValue: ((JoueurWithAvatar*) joueur).avatar forKey:@"avatar"];
    }
    
    NSError* error = nil;
    
    NSLog(@"Saving %@ Cash: %d", joueur.nom, joueur.cash);
    
    if(![context save:&error]) {
        NSLog(@"Problem with Core Data Saving");
    }
}

-(void) deletePlayer : (NSString*) nom {
    NSArray* players;
    NSManagedObjectContext* context = [self context];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"name = %@", nom];
    
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] initWithEntityName: @"Player"];
    fetchRequest.predicate = predicate;
    
    NSError* error = nil;
    players = [context executeFetchRequest:fetchRequest error:&error];
    if(players.count >0) {
        for(NSManagedObject* player in players) {
            NSLog(@"Deleting player : %@", [player valueForKey:@"name"]);
            [context deleteObject:player];
        }
    }
    
    if(![context save:&error]) {
        NSLog(@"Problem deleting player %@", nom);
    }
}
@end
