//
//  MonopolyQuerys.h
//  Monopoly
//
//  Created by Training on 18/09/2015.
//  Copyright (c) 2015 ValTechTraining. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JoueurWithAvatar.h"

@interface MonopolyQuerys : NSObject

-(NSMutableArray*) readPlayers;
-(void) writePlayer : (Joueur*) joueur;
-(void) deletePlayer : (NSString*) nom;

@end
